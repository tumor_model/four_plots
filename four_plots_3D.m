% This program attempts to recreate the four plots at the top of page 241
% in Kirschner and Panetta, (1998). These plots show the levels of effector
% cells, tumor cells, and IL-2 over time at four different values of the
% parameter c (the agenticity of the tumor).

% This version has 3D plots. Vera 3/5/17.

% Fixed parameters from Table 1 on page 138.
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
tumor_p.a = 1;
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% Add s_1 and s_2 to the tumor parameters. These are zero for now.
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

y0 = [1, 1, 1]; % Set initial values (x_0, y_0, z_0). Anything small and non-zero works

% Get the plot for c = 0.00005
tspan = [0, 200];
c = 0.00005;

% 3 dimensional version
fig1 = figure;
options = odeset('OutputFcn',@odephas3);
[t,y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0, options);
xlabel('Effector Cells (x)')
ylabel('Tumor Cells (y)')
zlabel('IL-2 (z)')
title('c = 0.00005 3D')

% Get the plot for c = 0.01
tspan = [0, 6000]; % Reduced range from 10000 in original plot to 6000 becuase ode23s can handle that
c = 0.01;

% 3 dimensional version
fig2 = figure;
options = odeset('OutputFcn',@odephas3);
[t,y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0, options);
xlabel('Effector Cells (x)')
ylabel('Tumor Cells (y)')
zlabel('IL-2 (z)')
title('c = 0.01 3D')

% Get the plot for c = 0.02
tspan = [0, 1000];
c = 0.02;

% 3 dimensional version
fig3 = figure;
options = odeset('OutputFcn',@odephas3);
[t,y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0, options);
xlabel('Effector Cells (x)')
ylabel('Tumor Cells (y)')
zlabel('IL-2 (z)')
title('c = 0.02 3D')

% Get the plot for c = 0.035
tspan = [0, 1000];
c = 0.035;

% 3 dimensional version
fig4 = figure;
options = odeset('OutputFcn',@odephas3);
[t,y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0, options);
xlabel('Effector Cells (x)')
ylabel('Tumor Cells (y)')
zlabel('IL-2 (z)')
title('c = 0.035 3D')
