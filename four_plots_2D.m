% This program attempts to recreate the four plots at the top of page 241
% in Kirschner and Panetta, (1998). These plots show the levels of effector
% cells, tumor cells, and IL-2 over time at four different values of the
% parameter c (the agenticity of the tumor).

% For the first and second plots, the behavior I am getting does not quite
% match the paper. Vera 2/21/17.

% Fixed parameters from Table 1 on page 138.
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
tumor_p.a = 1;
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% Add s_1 and s_2 to the tumor parameters. These are zero for now.
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

y0 = [1, 1, 1]; % Set initial values (x_0, y_0, z_0). Anything small and non-zero works

% Get the plot for c = 0.00005
tspan = [0, 200];
c = 0.00005;

[t, y] = ode45(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0);

fig1 = figure;
plot(t, y)
title('c = 0.00005')
xlabel('Time in Days')
ylabel('Volume')
set(gca, 'YScale', 'Log')
legend('Effector Cells', 'Tumor Cells', 'IL-2')

% Get the plot for c = 0.01
tspan = [0, 6000]; % Reduced range from 10000 in original plot to 6000 becuase ode23s can handle that
c = 0.01;

[t, y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0);
% With c at this value, the system is very, very stiff. ode45 can't handle
% it, and ode15s could only handle two spikes. ode23s can work when the
% range is limited to 6000, so that is what I did for now. Vera 2/24/17.

% If we continue with this, I may want to use odeset to specify the
% jacobian at some point in the future.

fig2 = figure;
plot(t, y)
title('c = 0.01')
xlabel('Time in Days')
ylabel('Volume')
legend('Effector Cells', 'Tumor Cells', 'IL-2')
ylim([0, 140000000])


% Get the plot for c = 0.02
tspan = [0, 1000];
c = 0.02;

[t, y] = ode45(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0);

fig3 = figure;
plot(t, y)
title('c = 0.02')
xlabel('Time in Days')
ylabel('Volume')
legend('Effector Cells', 'Tumor Cells', 'IL-2')

% Get the plot for c = 0.035
tspan = [0, 1000];
c = 0.035;

[t, y] = ode45(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0);

fig4 = figure;
plot(t, y)
title('c = 0.035')
xlabel('Time in Days')
ylabel('Volume')
legend('Effector Cells', 'Tumor Cells', 'IL-2')



